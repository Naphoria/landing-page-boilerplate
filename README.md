# Landing Page Boilerplate

The project has the following features:

 - ES6 into ES5 conversion
 - SASS
 - Bootstrap 4
 - Image optimization
 - Dev server
 - HMR
  
## Install
1. Clone the project
2. cd /project
3. Open terminal
4. Run `npm i`

## Dev server

Run `npm run start` to start to webpack dev server with HMR ready

## Production server

Run `npm run build` to build project's all assets in `dist` folder.